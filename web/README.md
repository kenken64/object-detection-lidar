## Vetex Shader

The Vertex Shader is the programmable Shader stage in the rendering pipeline that handles the processing of individual vertices. Vertex shaders are fed Vertex Attribute data, as specified from a vertex array object by a drawing command.

## File Structure:

- `data/`: contains the recorded scan data used to visualize the algorithms (json data stored as string in js file).
- `js`: the source code for the application
  - `components/`: the applications components/modules
  - `namespace.js`: defines the application namespace + structure of components/modules
  - `main.js`: control logic and entry point for the application
- `lib`: third party libraries
- `style`: css helpers
- `index.html`: the HTML page
