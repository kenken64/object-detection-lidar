from pymongo import MongoClient
import urllib.parse
import math

# area that the laser discover the dots
kMaxLaserDistance = 5 * 100.

angle = 307562
kDegreeToRadian = 0.017453292519943295
degree = (angle /1000 + 90) % 360
radian = degree * kDegreeToRadian
distance = 256

## from angle / distance to cartesian
x = math.cos(radian) * distance
y = math.sin(radian) * distance

x  = x + kMaxLaserDistance
y  = y + kMaxLaserDistance
print('x- {}   y - {}'.format(x,y))

print(kMaxLaserDistance)

#username = urllib.parse.quote_plus("admin")
#password = urllib.parse.quote_plus("password1234")
#print('mongodb+srv://%s:%s@cluster0-cv7dc.mongodb.net/test?retryWrites=true&w=majority' %(username, password))
#client = MongoClient('mongodb+srv://%s:%s@cluster0-cv7dc.mongodb.net/test?retryWrites=true&w=majority' %(username, password))
client = MongoClient('mongodb://localhost:27017/')

client.list_database_names()
db = client.lidar_db
sweepdp = db.sweep_datapoints
data = {
	"angle": angle,
	"kMaxLaserDistance": kMaxLaserDistance,
	"kDegreeToRadian": kDegreeToRadian,
	"distance": distance,
	"x": x,
	"y": y,
	"degree": degree,
	"radian": radian
}
sweep_id= sweepdp.insert_one(data).inserted_id
print(sweep_id)
