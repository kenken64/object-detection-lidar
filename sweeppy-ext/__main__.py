import itertools
import sys
from . import Sweep
from pymongo import MongoClient
import urllib.parse
import math
import datetime
import time

#username = urllib.parse.quote_plus("admin")
#password = urllib.parse.quote_plus("password1234")
#print('mongodb+srv://%s:%s@cluster0-cv7dc.mongodb.net/test?retryWrites=true&w=majority' %(username, password))
#client = MongoClient('mongodb+srv://%s:%s@cluster0-cv7dc.mongodb.net/test?retryWrites=true&w=majority' %(username, password))
client = MongoClient('mongodb://localhost:27017/')

client.list_database_names()
db = client.lidar_db
sweepdp = db.sweep_datapoints

MAX_COUNT = 310
batchData = []
counter = MAX_COUNT

def main():
   
    if len(sys.argv) < 2:
        sys.exit('python -m sweeppy /dev/ttyUSB0')

    dev = sys.argv[1]

    with Sweep(dev) as sweep:
        speed = sweep.get_motor_speed()
        rate = sweep.get_sample_rate()

        print('Motor Speed: {} Hz'.format(speed))
        print('Sample Rate: {} Hz'.format(rate))

        # Starts scanning as soon as the motor is ready
        sweep.start_scanning()

        # get_scans is coroutine-based generator lazily returning scans ad infinitum
        #for scan in itertools.islice(sweep.get_scans(), 320):
        for scan in sweep.get_scans():
            print('................Scan......................')
            print(scan)
            print('..................Scan samples.......................')
            print(scan.samples)
            print('....................Len.....................')
            print(len(scan.samples))
            storeToMongoDb(scan.samples)

def storeToMongoDb(samples):
    global batchData
    global counter

    for sample in samples:
        # area that the laser discover the dots
        kMaxLaserDistance = 5 * 100.

        angle = sample.angle
        kDegreeToRadian = 0.017453292519943295
        degree = (angle /1000 + 90) % 360
        radian = degree * kDegreeToRadian
        distance = sample.distance

        ## from angle / distance to cartesian
        x = math.cos(radian) * distance
        y = math.sin(radian) * distance

        x  = x + kMaxLaserDistance
        y  = y + kMaxLaserDistance
        print('x- {}   y - {}'.format(x,y))

        print(kMaxLaserDistance)

        
        data = {
            "angle": angle,
            "kMaxLaserDistance": kMaxLaserDistance,
            "kDegreeToRadian": kDegreeToRadian,
            "distance": distance,
            "x": x,
            "y": y,
            "degree": degree,
            "radian": radian,
            "signal_strength": sample.signal_strength
        }
        
        counter = counter - 1
        print(counter)
        if(counter < 0):
            finalData = {
                "timeStamp": datetime.datetime.now(),
                "data": batchData,
                "max_count": len(batchData)
            }
            sweep_id= sweepdp.insert_one(finalData).inserted_id
            print(sweep_id)
            batchData = []
            counter = MAX_COUNT
        else: 
            batchData.append(data)
            print(len(batchData))
        
main()
